Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

  * Ross Gammon (rossgammon)
  * Balasankar C (balasankarc)
  * Roland Fehrenbacher (rfehren)
  * Jonathan Cristopher Carter (jcc)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

  * José Gutiérrez de la Concha
  * Paolo Greppi
  * Ming-ting Yao Wei
  * Boyuan Yang
  * Paul Hardy
  * Fabian Wolff
  * Moritz Schlarb
  * Shengjing Zhu


Félicitations !
