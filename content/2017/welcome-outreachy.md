Title: Debian welcomes its Outreachy interns
Slug: welcome-outreachy-interns-2016-2017
Date: 2017-02-05 12:00
Author: Nicolas Dandrimont and Laura Arjona Reina 
Tags: announce, outreachy
Status: published

![Outreachy logo](|filename|/images/outreachy-logo-300x61.png)

Better late than never, we'd like to welcome our three Outreachy interns 
for this round, lasting from the 6th of December 2016 to the 6th of March 2017.


[Elizabeth Ferdman](https://eferdman.github.io/) 
is working in the 
[Clean Room for PGP and X.509 (PKI) Key Management](https://wiki.debian.org/Outreachy/Round13#Outreachy.2FRound13.2FProjects.2FCleanRoomForPGPKeyManagement.Clean_Room_for_PGP_and_X.509_.28PKI.29_Key_Management).


[Maria Glukhova](https://siamezzze.github.io/) 
is working in 
[Reproducible builds for Debian and free software](https://wiki.debian.org/Outreachy/Round13#Outreachy.2FRound13.2FProjects.2FReproducibleBuildsOfDebian.Reproducible_builds_for_Debian_and_free_software).


[Urvika Gola](https://urvikagola.wordpress.com/) 
is working in 
[improving voice, video and chat communication with free software](https://wiki.debian.org/Outreachy/Round13#Outreachy.2FRound13.2FProjects.2FImprovingVoiceVideoChatCommunication.Improving_voice.2C_video_and_chat_communication_with_free_software).


From [the official website](https://www.gnome.org/outreachy): *Outreachy 
helps people from groups underrepresented in free and open source software 
get involved. We provide a supportive community for beginning to contribute 
any time throughout the year and offer focused internship opportunities 
twice a year with a number of free software organizations.*



The Outreachy program is possible in Debian thanks
to the effort of Debian developers and contributors that dedicate
part of their free time to mentor students and outreach tasks, and 
the help of the [Software Freedom Conservancy](https://sfconservancy.org/), 
who provides administrative support for Outreachy, 
as well as the continued support of Debian's donors, who provide funding 
for the internships. 


Debian will also participate in the next round for Outreachy, during
the summer of 2017. More details will follow in the next weeks.


Join us and help extend Debian! You can follow the work of the Outreachy
interns reading their blogs (they are syndicated in [Planet Debian][planet]), 
and chat with us in the #debian-outreach IRC channel and [mailing list](https://lists.debian.org/debian-outreach/).

[planet]: https://planet.debian.org 

Congratulations, Elizabeth, Maria and Urvika!
