Title: Novos desenvolvedores e mantenedores Debian (janeiro e fevereiro de 2017)
Slug: new-developers-2017-02
Date: 2017-03-08 00:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian 
nos últimos dois meses:

  * Ulrike Uhlig (ulrike)
  * Hanno Wagner (wagner)
  * Jose M Calhariz (calharis)
  * Bastien Roucariès (rouca)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian 
nos últimos dois meses:

  * Dara Adib
  * Félix Sipma
  * Kunal Mehta
  * Valentin Vidic
  * Adrian Alves
  * William Blough
  * Jan Luca Naumann
  * Mohanasundaram Devarajulu
  * Paulo Henrique de Lima Santana
  * Vincent Prat

Parabéns a todos!
