Title: Novos desenvolvedores e mantenedores Debian (março e abril de 2017)
Slug: new-developers-2017-04
Date: 2017-05-15 12:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian 
nos últimos dois meses:

  * Guilhem Moulin (guilhem)
  * Lisa Baron (jeffity)
  * Punit Agrawal (punit)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian 
nos últimos dois meses:

  * Sebastien Jodogne
  * Félix Lechner
  * Uli Scholler
  * Aurélien Couderc
  * Ondřej Kobližek
  * Patricio Paez

Parabéns a todos!
