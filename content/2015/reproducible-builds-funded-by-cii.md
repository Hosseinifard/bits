Title: Reproducible Builds get funded by the Core Infrastructure Initiative
Slug: reproducible-builds-funded-by-cii
Date: 2015-06-23 14:00
Author: Ana Guerrero Lopez
Tags: debian, cii, reproducible builds
Status: published


The Core Infrastructure Initiative [announced][lf-announce] today that they will
support two Debian Developers, Holger Levsen and Jérémy Bobbio, with $200,000
to advance their Debian work in reproducible builds and to collaborate more
closely with other distributions such as Fedora, Ubuntu, OpenWrt to benefit
from this effort.

[lf-announce]:http://www.linuxfoundation.org/news-media/announcements/2015/06/linux-foundation-s-core-infrastructure-initiative-funds-three-new

The [Core Infrastructure Initiative][cii-link] (CII) was established in 2014 to
fortify the  security of key open source  projects. This initiative is funded by
more than 20 companies and managed by The Linux Foundation.
[cii-link]: http://www.linuxfoundation.org/programs/core-infrastructure-initiative

The [reproducible builds][rb] initiative aims to  enable anyone to  reproduce
bit by bit identical binary packages from a given source, thus enabling anyone
to independently verify that a binary matches the  source code from which it
was said it was derived. For example, this allow the users of Debian to rebuild
packages and obtain exactly identical packages to the ones provided by the
Debian repositories.
[rb]:https://wiki.debian.org/ReproducibleBuilds



