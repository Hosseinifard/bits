Title: Bầu cử lãnh đạo dự án Debian 2016
Date: 2016-03-27 20:55
Tags: dpl, vote
Slug: dpl-elections-2016
Author: Ana Guerrero Lopez
Translator: Lê Thị Minh
Lang: vi
Status: published

Lại một năm đối với dự án Debian: [bầu cử người lãnh đạo dự án](https://www.debian.org/vote/2016/vote_001)!

Neil McGovern người được chọn làm lãnh đạo năm ngoái sẽ không được tái cử.
Các nhà phát triển Debian sẽ có hai lựa chọn, một là bầu cho ứng viên duy nhất
[Mehdi Dogguy](https://www.debian.org/vote/2016/platforms/mehdi)
hai là *Không chọn ai*. Nếu kết quả bầu cử nghiêng về *Không chọn ai* thì quy
trình bầu cử sẽ cứ thế lặp lại.

Mehdi Dogguy là ứng viên của vị trí DPL năm ngoái, kết quả xếp thứ hai với số
lượng bầu chọn xấp xỉ người thắng cuộc Neil McGovern.

Chúng ta đang trong giai đoạn trung kỳ của chiến dịch và sẽ kéo dài đến ngày 2
tháng 4. Mong rằng các ứng viên và các đóng góp viên Debian sẽ tham gia vào các
cuộc tranh luận và bàn bạc về [bó thư bầu cử debian](http://lists.debian.org/debian-vote/).

Giai đoạn bầu cử bắt đầu vào ngày 3 tháng 4, và trong vòng 2 tuần , các nhà phát
triển Debian sẽ bầu chọn người dẫn dắt dự án trong thời gian 1 năm. Kết quả sẽ
được công khai vào ngày 17/4, thời kỳ của người lãnh đạo mới sẽ có hiệu lực cùng ngày.
