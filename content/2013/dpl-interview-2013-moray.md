Title: Debian Project Leader elections 2013: interview with Moray Allan
Date: 2013-03-27 18:54
Tags: interview, dpl, vote
Slug: dpl-interview-2013-moray
Author: Francesca Ciceri
Status: published


We have asked Moray Allan, one of the three candidates for 
[DPL elections 2013](|filename|candidates-for-dpl-2013.md),
to tell our readers about himself and his ideas for the Debian Project.

You can also read the interviews to the other two candidates: 
[Lucas Nussbaum](|filename|dpl-interview-2013-lucas.md) 
and [Gergely Nagy](|filename|dpl-interview-2013-algernon.md).

----

**Please tell us a little about yourself.**

I'm Moray Allan, from Edinburgh in Scotland.  I'm 32.  After working in 
academic research for a few years, I'm now working freelance on a wide 
mixture of topics, with recent projects in Indonesia, Romania and 
Kuwait.  When I'm not working, I'm likely to be found walking through a 
city or the countryside, or otherwise relaxing at home reading a novel 
in French or Spanish.

**What do you do in Debian and how did you started contributing?**

In recent years, most of my Debian time was taken up organising the 
annual Debian conferences.  But I still have a load of packages, mostly 
connected to an upstream Linux-on-handheld-computers project I was 
working on before I joined Debian to create packages for it.

**Why did you decide to run as DPL?**

I've been involved in Debian for about 10 years now, including working 
for the last few years in DebConf in a way similar to how the DPL acts 
within overall Debian.  Previously I'd ruled out running due to lack of 
time, but currently I'm in a more flexible work situation.  It seems the 
right time to put myself forward, and see if the ideas in my platform 
interest project members.

**Three keywords to summarise your platform.**

Transparency, communication, openness.  (Three ways I'd like us to 
think about teams in Debian.)

**What are the biggest challenges that you envision for Debian in the future?**

I think the biggest challenges are for free software in general.  
End-users are moving to more closed hardware -- will our software be 
able to run on the phones and tablets people are shifting towards?  At 
the same time, end-users and server users are moving to "the cloud", and 
often depending more heavily on non-free infrastructure outside their 
own control.

**What are, in your opinion, the areas of the project more in need 
of technical and/or social improvements?**

In my platform I give a few ideas about teams and delegations, 
coordination and mediation, and both internal and external 
communication, including more organised fundraising.  These are areas 
where I think relatively simple changes can give big benefits.

**Why should people vote for you?**

I have proven leadership experience within Debian, as I've been working 
on coordination and mediation tasks for some years already.  At the same 
time, I do regular packaging work, and work in other parts of Debian 
like the press and publicity teams, so I'm in touch with the experience 
of normal Debian contributors.  People should vote for me if they 
support my platform, which is about coordination-level changes that I 
would have no mandate or authority to push through unless I am elected.

**Name three tools you couldn't stay without.**

APT, emacs, ssh.

**What keep you motivated to work in Debian?**

I've used Debian on all my computers for a long time, and by now 
working on the distribution myself feels a natural part of that.  
Fortunately I'm constantly positively surprised by Debian and by the 
Debian community.

**Are there any other fields where you call yourself a geek, besides computers?**

Certainly history (such as the eastern Mediterranean region in late 
antiquity), languages (including dead ones) and music (especially 
Josquin to Monteverdi).
