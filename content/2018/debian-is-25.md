Title: 25 years and counting
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Status: published

![Debian is 25 years old by Angelo Rosa](|filename|/images/debian25years.png)

When the late Ian Murdock announced 25 years ago in comp.os.linux.development,
*"the imminent completion of a brand-new Linux release, [...] the Debian Linux Release"*,
nobody would have expected the "Debian Linux Release" to become what's nowadays
known as the Debian Project, one of the largest and most influential free software projects.
Its primary product is Debian, a free operating system (OS) for your computer, as well as
for plenty of other systems which enhance your life. From the inner workings of your nearby
airport to your car entertainment system, and from cloud servers hosting your favorite
websites to the IoT devices that communicate with them, Debian can power it all.

Today, the Debian project is a large and thriving organization with countless self-organized
teams comprised of volunteers. While it often looks chaotic from the outside, the project
is sustained by its two main organizational documents: the [Debian Social Contract], which provides
a vision of improving society, and the [Debian Free Software Guidelines], which provide an indication
of what software is considered usable. They are supplemented by the project's [Constitution] which
lays down the project structure, and the [Code of Conduct], which sets the tone for interactions within the project.

Every day over the last 25 years, people have sent bug reports and patches, uploaded
packages, updated translations, created artwork, organized events about Debian, updated
the website, taught others how to use Debian, and created hundreds of derivatives.

**Here's to another 25 years - and hopefully many, many more!**

[Debian Social Contract]: https://www.debian.org/social_contract
[Debian Free Software Guidelines]: https://www.debian.org/social_contract#guidelines
[Constitution]: https://www.debian.org/devel/constitution
[Code of Conduct]: https://www.debian.org/code_of_conduct
