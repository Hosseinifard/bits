Title: 新的 Debian 开发者和维护者 (2018年5月至6月)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Andre Bianchi
  * Simon Quigley
  * Andrius Merkys
  * Tong Sun
  * James Lu
  * Raphaël Halimi
  * Paul Seyfert
  * Dustin Kirkland
  * Yanhao Mo
  * Paride Legovini

祝贺他们！

另外，我们正在寻求更多的 Debian 成员成为申请管理员（Application Manager，AM）来处理其他贡献者的申请，从而帮助他们获得
Debian 开发者帐号。如需了解详情，请阅读[新成员前台团队发出的邮件](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html)。
